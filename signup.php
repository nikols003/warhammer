<?php
require 'db.php';

$data = $_POST;
if ( isset($data['do_signup']) )
{
    //check registration form
    $errors = array();
    if ( trim($data['login']) == '' )
    {
        $errors[] = 'Enter your login!';
    }

    if ( trim($data['email']) == '' )
    {
        $errors[] = 'Enter your E-mail!';
    }

    if ( $data['password'] == '' )
    {
        $errors[] = 'Enter your Password!';
    }

    if ( R::count('users', "login = ?", array($data['login'])) > 0 )
    {
        $errors[] = 'This login is already used!';
    }
    if ( R::count('users', "email = ?", array($data['email'])) > 0 )
    {
        $errors[] = 'This E-mail is already used!';
    }

    if (empty($errors))
    {
        //all is OK - registration;
        $user = R::dispense('users');
        $user->login = $data['login'];
        $user->email = $data['email'];
        $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
        R::store($user);
        echo '<div style="color: greenyellow" ">You signed-up !</div><hr/>';
        header('Location: login.php');
    }
    else
    {
        echo '<div style="color: red" "; margin-left: 0px; margin:auto;>'.array_shift($errors).'</div><hr/>';
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login for GAME</title>
    <style>
        body
        {
            background-image: url("source/img/Space-Wallpaper.jpg");
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            height: 1270px;

        }
        .login
        {
            width: 100%;
            height: 100%;
            position: relative;
            text-align: center;
        }
        .form
        {
            color: #FFFFFF;
            width: 30%;
            height: 23%;
            text-align: center;
            margin: auto;
            margin-top: 300px;
            padding: 10px;
        }
        .lfield
        {
            height: 100%;
            padding-top: 20px;
            background-color: rgba(22,22,22,0.55);
            border-radius: 40px 10px;
            box-shadow: 0 10px 10px black;
            position: relative;
        }
        input
        {
            height: 8%;
            width: 70%;
            text-align: center;
            font-size: 22px;
            margin-top: 10px;
            border-radius: 10px;
            box-shadow: 0 10px 10px black;
        }
        button
        {
            margin-top: 15px;
            width: 100px;
            height: 30px;
            border-radius: 10px 10px 10px 10px;
            background-color: rgba(22,22,22,0.7);
            box-shadow: 0 10px 10px black;
            color: white;
            font-size: 14px;
        }
        img
        {
            width: 60px
        }
    </style>
</head>
    <body>
        <div class="login">
            <div class="form">
                <div class="lfield">

                    <form action="signup.php" method="POST">

                        <p>
                            <input type="text" name="login" placeholder="Login" value="<?php echo @$data['login']; ?>">
                        </p>
                        <p>
                            <input type="email" name="email" placeholder="E-mail" value="<?php echo @$data['email']; ?>">
                        </p>
                        <p>
                            <input type="password" name="password" placeholder="password" value="">
                        </p>
                        <p>
                            <button type="submit" name="do_signup">
                                Sign Up
                            </button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>