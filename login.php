<?php
require 'db.php';

$data = $_POST;
if ( isset($data['do_login']) )
{
    $errors = array();
    $user = R::findOne('users', 'login = ?', array($data['login']));
    if ( $user )
    {
        //LOGIN FOUND check pass
        if ( password_verify($data['password'], $user->password) )
        {
            //Пароль подтвержден
            $_SESSION['logged_user'] = $user;
            //echo '<div style="color: greenyellow" ">Welcome! Come to <a href="index.php">main page</a>></div><hr/>';
            header('Location: source/try_chat.php');
        }
        else
        {
            $errors[] = "Incorect password!";
        }
    }
    else
    {
        $errors[] = "User with this login not found!";
    }
    if ( ! empty($errors) )
    {
        echo '<div style="color: red" ">'.array_shift($errors).'</div><hr/>';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login for GAME</title>

    <style>
        body
        {
            background-image: url("source/img/Space-Wallpaper.jpg");
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            height: 1270px;

        }
        form
        {
            color: #FFFFFF;
            width: 25%;
            height: 15%;
            text-align: center;
            margin: auto;
            margin-top: 300px;
            padding: 10px;
        }
        input
        {
            height: 17%;
            width: 70%;
            text-align: center;
            font-size: 22px;
            margin-top: 10px;
            border-radius: 10px;
            box-shadow: 0 10px 10px black;
        }
        button
        {
            margin-top: 20px;
            width: 35%;
            height: auto;
            border-radius: 10px 10px 10px 10px;
            background-color: black;
            box-shadow: 0 10px 10px black;
            color: white;
            font-size: 18px;
        }
        img
        {
            width: 60px
        }
    </style>
</head>
<body>
<form action="login.php" method="POST">
    <p>
        <input type="text" name="login" placeholder="Login" value="<?php echo @$data['login']; ?>">
    </p>
    <p>
        <input type="password" name="password" placeholder="password" value="<?php echo @$data['password']; ?>">
    </p>
    <p>
        <button type="LogIn" name="do_login">
            LogIn
        </button>
    </p>

</form>
</body>
</html>
