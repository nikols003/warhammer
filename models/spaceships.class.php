<?php


require_once 'weapon.class.php';

class Spaceship
{
	public $name;
	public $size;
	public $HP;
	public $PP;
	public $speed;
	public $c_speed;
	public $handling;
	public $bouclier;
	public $weapon;
	public $shield;
	public $c_shield;
	public $count_weapon = 1;
	public $id;
	static $count = 1;

	function __construct($name_ship)
	{
		if (method_exists($this, $name_ship)) {
			$this->$name_ship();
			$this->id = self::$count;
			self::$count++;
		}
		else
			echo "Wrong name of spaceship!!!".PHP_EOL;
		$this->c_shield = $this->shield;
		$this->c_speed = $this->speed;
		$this->weapon->c_charge = $this->weapon->charge;
	}

	function Imperial_Frigate_1 ()
	{
		$this->name = "Honorable Duty";
		$this->size = "14";
		$this->HP = 5;
		$this->PP = 10;
		$this->speed = 15;
		$this->handling = 4;
		$this->shield = 0;
		$this->weapon = new Weapon("Side laser batteries");
	}

	function Imperial_Destroyer ()
	{
		$this->name = "Sword Of Absolution";
		$this->size = "13";
		$this->HP = 4;
		$this->PP = 10;
		$this->speed = 18;
		$this->handling = 3;
		$this->shield = 0;
		$this->weapon = new Weapon("Side laser batteries");
	}

	function Imperial_Frigate_2 ()
	{
		$this->name = "Honorable Duty";
		$this->size = "14";
		$this->HP = 5;
		$this->PP = 10;
		$this->speed = 15;
		$this->handling = 4;
		$this->shield = 0;
		$this->weapon = new Weapon("Nautical lance");
	}

	function Imperial_Ironclad ()
	{
		$this->name = "Imperator Deus";
		$this->size = "27";
		$this->HP = 8;
		$this->PP = 12;
		$this->speed = 10;
		$this->handling = 5;
		$this->shield = 2;
		$this->count_weapon = 2;
		$this->weapon = new Weapon("Nautical lance");
	}

	function Onslaught_Attack_Ship ()
	{
		$this->name = "Orktobre Roug";
		$this->size = "12";
		$this->HP = 4;
		$this->PP = 10;
		$this->speed = 19;
		$this->handling = 3;
		$this->shield = 0;
		$this->weapon = new Weapon("Side laser batteries");
	}

	function Terror_Ship ()
	{
		$this->name = "Ork’N’Roll !";
		$this->size = "15";
		$this->HP = 6;
		$this->PP = 10;
		$this->speed = 12;
		$this->handling = 4;
		$this->shield = 0;
		$this->weapon = new Weapon("Close range super heavy automatic weapon");
	}

	function move($x, $y, Arena $a)
	{
		$flag = 1;
		if ($x < 1 || $x > 100 || $y < 1 || $y > 150) {
			$_SESSION["error"][] = "Ship #$this->id incorrect coordinate";
			return;
		}
		$i = 1;
		$k = $x;
		while ($i < $this->size / 10 && $k < $a->y)
		{
			$j = 1;
			$l = $y;
			while ($j < $this->size % 10 && $l < $a->x)
			{
				if ($a->arena[$k][$l] > 0) {
					$_SESSION["error"][] = "$k-$l not empty coordinate";
					return;
				}
				$j++;
				$l++;
			}
			$i++;
			$k++;
		}
		for ($i = 0; $i < $a->y; $i++)
		{
			for ($j = 0; $j < $a->x; $j++)
			{
				if ($a->arena[$i][$j] == $this->id && $flag)
				{
					$range = (int)sqrt(pow($x - $i, 2) + pow($y - $j, 2));
					if ($range > $this->c_speed)
					{
						$_SESSION["error"][] = "Ship ID#$this->id can't move to $x-$y his range move only $this->c_speed";
						return ;
					}
					$flag = 0;
				}
				if ($a->arena[$i][$j] == $this->id)
					$a->arena[$i][$j] = 0;
			}
		}
		$i = 1;
		$k = $x;
		$this->c_speed = $this->speed;
		while ($i < $this->size / 10 && $k < $a->y)
		{
			$j = 0;
			$l = $y;
			while ($j < $this->size % 10 && $l < $a->x)
			{
				$a->arena[$k][$l] = $this->id;
				$j++;
				$l++;
			}
			$i++;
			$k++;
		}
		$_SESSION['error'][] = "Ships #$this->id go to $x-$y";
	}

	function clean_param()
	{
		$this->c_speed = $this->speed;
		$this->c_shield = $this->shield;
		$this->weapon->c_charge = $this->weapon->charge;
	}

	function find_coor(Arena $a)
	{
		$i = 0;
		while ($i < $a->y)
		{
			$j = 0;
			while ($j < $a->x)
			{
				if ($a->arena[$i][$j] == $this->id)
					return (array($i, $j));
				$j++;
			}
			$i++;
		}
	}

	function fire($x, $y, Arena $a)
	{
		if ($this->weapon->c_charge < 1) {
			$_SESSION['error'][] = "Weapon have not charge!!! ";
			return array();
		}
		if ($x < 1 || $y < 1)
		{
			$_SESSION['error'][] = "Empty coordinate!!!";
			return array();
		}
		$ar = $this->find_coor($a);
		$range = (int)sqrt(pow($x - $ar[0], 2) + pow($y - $ar[1], 2));
		if ($range > $this->weapon->range[2][1])
		{
			$_SESSION['error'][] = "Ship #$this->id cat't fire this range!!!";
			return array();
		}
		if ($range < $this->weapon->range[0][1])
			$cof = 1;
		elseif ($range < $this->weapon->range[1][1])
			$cof = 2;
		else
			$cof = 1;
		if ($a->arena[$x][$y] < 1)
		{
			$_SESSION['error'][] = "On coordinate $x-$y empty!(((";
			return array();
		}
		else
		{
			$fire = (int)($cof * $this->weapon->c_charge / rand(1,6));
			$_SESSION['error'][] = "Ships id#".$a->arena[$x][$y]." received damage $fire";
			return (array('id' => $a->arena[$x][$y], 'damage' => $fire));
		}

	}

	function pp($speed, $shield, $charge)
	{
		if (!$speed && !$shield && !$charge) {
			$_SESSION['error'][] = "Ships id#$this->id att didn't increase!!!";
			return ;
		}
		if ($speed + $charge + $shield > $this->PP)
			$_SESSION['error'][] = "Ships id#$this->id have only $this->PP power-point";
		$_SESSION['error'][] = "Ships id#$this->id increase next att: speed + $speed, shield + $shield, charge + $charge";
		$this->c_speed += $speed;
		$this->c_shield += $shield;
		$this->weapon->c_charge += $charge;
	}
}
