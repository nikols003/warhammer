<?php


class Weapon
{
	public $charge;
	public $c_charge;
	public $range;
	//effect zone

	function __construct($weapon_name)
	{
		try {
			if ($weapon_name == "Side laser batteries") {
				$this->charge = 0;
				$this->range[0] = array(1, 10);
				$this->range[1] = array(11, 20);
				$this->range[2] = array(21, 30);
			} elseif ($weapon_name == "Nautical lance") {
				$this->charge = 0;
				$this->range[0] = array(1, 30);
				$this->range[1] = array(31, 60);
				$this->range[2] = array(61, 90);
			} elseif ($weapon_name == "Heavy nautical lance") {
				$this->charge = 3;
				$this->range[0] = array(1, 30);
				$this->range[1] = array(31, 60);
				$this->range[2] = array(61, 90);
			} elseif ($weapon_name == "Close range super heavy automatic weapon") {
				$this->charge = 5;
				$this->range[0] = array(1, 3);
				$this->range[1] = array(4, 7);
				$this->range[2] = array(8, 10);
			} elseif ($weapon_name == "Macro canon") {
				$this->charge = 0;
				$this->range[0] = array(1, 10);
				$this->range[1] = array(11, 20);
				$this->range[2] = array(21, 30);
			} else
				throw new \Exception("Error: incorrect weapon" . PHP_EOL);
		} catch (\Exception $e) {
			echo $e->getMessage();
		}

	}
}
