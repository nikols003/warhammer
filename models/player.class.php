<?php
/**
 * Created by PhpStorm.
 * User: mdubina
 * Date: 4/14/17
 * Time: 10:49 AM
 */


class Player
{
	public $name;
	public $ships;
	public $id;

	function __construct(array $data)
	{
		$this->name = $data['name'];
		foreach ($data['ships'] as $ship) {
			$this->ships[] = new Spaceship($ship);
		}
		$this->id = $data['id'];

	}


	function delete_kill_ships(Arena $a)
	{
		$i = 0;
		while ($i < 6)
		{
			if (isset($this->ships[$i]) && $this->ships[$i]->HP < 1)
			{
//				echo "Delete this ship".$this->ships[$i]->id." ".$this->ships[$i]->HP.PHP_EOL;
				for ($j = 0; $j < $a->y; $j++)
				{
					for ($k = 0; $k < $a->x; $k++)
					{
						if ($a->arena[$j][$k] == $this->ships[$i]->id)
							$a->arena[$j][$k] = 0;
					}
				}
				$_SESSION['error'][] = "Delete this ship".$this->ships[$i]->id." ".$this->ships[$i]->HP;
				unset($this->ships[$i]);
			}
			$i++;
		}

	}

	function put_ship_on_arena(Arena $a)
	{
		foreach ($this->ships as $ship) {
			$a->put_ship($ship, $this->id);
		}
	}

	function info()
	{
		?>
		<b>Player name: <?=$this->name?> </b><br>
		<?php
		foreach ($this->ships as $ship)
		{
			?>
			<b>Ship - <?=$ship->name?> ID - <?=$ship->id?></b> <br>
			HP - <?=$ship->HP?>, PP - <?=$ship->PP?>, speed - <?=$ship->c_speed?>,
			shield -
			<?=$ship->c_shield?><br>
			weapon: charge - <?=$ship->weapon->c_charge?>, short: <?=$ship->weapon->range[0][0]?> -
			<?=$ship->weapon->range[0][1]?>, middle: <?=$ship->weapon->range[1][0]?> -
			<?=$ship->weapon->range[1][1]?>, long: <?=$ship->weapon->range[2][0]?> -
			<?=$ship->weapon->range[2][1]?> <br>
			_____________________________ <br>
		<?php
		}
	}
}