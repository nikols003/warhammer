<?php
session_start();
if (isset($_POST['submit']) && $_POST['submit'] =="OK")
{
	$_SESSION['start'] = $_POST;
	header('Location: ../index2.php');
}



$opt = <<<HTML
				<option value="Imperial_Frigate_1">Imperial_Frigate_1</option>
				<option value="Imperial_Destroyer">Imperial_Destroyer</option>
				<option value="Imperial_Frigate_2">Imperial_Frigate_2</option>
				<option value="Imperial_Ironclad">Imperial_Ironclad</option>
				<option value="Onslaught_Attack_Ship">Onslaught_Attack_Ship</option>
				<option value="Terror_Ship">Terror_Ship</option>
HTML;

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<style>
		select{
			font-size: 12px;
			margin: 2px 0;
		}
	</style>
	<meta charset="UTF-8">
	<title>Start</title>
</head>
<body>
<form action="start.php" method="post">
	<div style="width: 95%; height: 300px; font-size: 24px">
		<div style="float: left; margin: 15px">
			Player #1 name: <br>
			<input type="text" name="p1_name"> <br>
			Ваши корабли: <br>
			<select   name="ship_1">
				<?=$opt?>
			</select><br>
			<select name="ship_2">
				<?=$opt?>
			</select><br>
			<select name="ship_3">
				<?=$opt?>
			</select><br>
			<select name="ship_4">
				<?=$opt?>
			</select><br>
			<select name="ship_5">
				<?=$opt?>
			</select><br>
			<select name="ship_6">
				<?=$opt?>
			</select><br>
		</div>
		<div style="float: left; margin: 15px">
			Player #2 name: <br>
			<input type="text" name="p2_name"> <br>
			Ваши корабли: <br>
			<select name="ship_7">
				<?=$opt?>
			</select><br>
			<select name="ship_8">
				<?=$opt?>
			</select><br>
			<select name="ship_9">
				<?=$opt?>
			</select><br>
			<select name="ship_10">
				<?=$opt?>
			</select><br>
			<select name="ship_11">
				<?=$opt?>
			</select><br>
			<select name="ship_12">
				<?=$opt?>
			</select><br>
		</div>
	</div>
	<div style="margin-left: 100px"><input style="width: 100px" type="submit" name="submit" value="OK"></div>
</form>
</body>
</html>
