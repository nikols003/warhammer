<?php
require '../db.php';
    if (!file_exists('chat'))
    {
        return ;
        header('Location: index.php');
    }
    date_default_timezone_set('Europe/Kiev');
    $messages = unserialize(file_get_contents('chat'));
    foreach ($messages as $elem)
    {
        echo "[".date('H:i:s', $elem['time'])."]<b>".$elem['login']."</b>: ".$elem['message']."<br />";
    }
?>
