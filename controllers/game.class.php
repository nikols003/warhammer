<?php
/**
 * Created by PhpStorm.
 * User: mdubina
 * Date: 4/14/17
 * Time: 11:08 AM
 */

require_once 'views/page_content.php';

class Game
{
	use page_content;
	public $p1;
	public $p2;
	public $all_ships;
	public $a;

	function __construct($data)
	{
		$this->p1 = $data[0];
		$this->p2 = $data[1];
		$this->a = new Arena();
		$this->p1->put_ship_on_arena($this->a);
		$this->p2->put_ship_on_arena($this->a);
		$_SESSION['game'] = $this;
		$_SESSION['phase'] = 1;
		$_POST['submit'] = "PP";
		$this->put_page_content();
	}

	function move($data)
	{
		$this->all_ships = array_merge($this->p1->ships, $this->p2->ships);
		foreach ($this->all_ships as $key => $ship)
			$ship->move($data['ship_'.$ship->id.'_x'], $data['ship_'.$ship->id.'_y'], $this->a);
		$_SESSION['phase'] = 1;
		$_SESSION['game'] = $this;
		$this->put_page_content();
	}

	function fire($data)
	{
		$this->all_ships = array_merge($this->p1->ships, $this->p2->ships);
		foreach ($this->all_ships as $key => $ship) {
			$array = $ship->fire($data['ship_' . $ship->id . '_x'], $data['ship_' . $ship->id . '_y'], $this->a);
			if (key_exists('id', $array))
			{
				$i = 0;
				while (isset($this->all_ships[$i]))
				{
					if ($this->all_ships[$i]->id == $array['id'])
					{
						if ($this->all_ships[$i]->c_shield == 0)
							$this->all_ships[$i]->HP -= $array['damage'];
						else
						{
							if ($this->all_ships[$i]->c_shield >= $array['damage'])
								$this->all_ships[$i]->c_shield -= $array['damage'];
							else
							{
								$array['damage'] -= $this->all_ships[$i]->c_shield;
								$this->all_ships[$i]->c_shield = 0;
								$this->all_ships[$i]->HP -= $array['damage'];
							}
						}
					}
					$i++;
				}
			}
		}
		$this->p1->delete_kill_ships($this->a);
		$this->p2->delete_kill_ships($this->a);
		if (!count($this->p2->ships)) {
			$_SESSION['win'] = $this->p2->name;
			header('Location: win.php');
		}
		if (!count($this->p1->ships)) {
			$_SESSION['win'] = $this->p1->name;
			header('Location: win.php');
		}
		$_SESSION['phase'] = 1;
		$_SESSION['game'] = $this;
		$this->put_page_content();
	}

	function pp($data)
	{
		$this->all_ships = array_merge($this->p1->ships, $this->p2->ships);
		foreach ($this->all_ships as $key => $ship) {
			$ship->clean_param();
			$ship->pp($data['ship_'.$ship->id.'_1'], $data['ship_'.$ship->id.'_2'], $data['ship_'.$ship->id.'_3']);
		}
		$this->put_page_content();
		$_SESSION['game'] = $this;
	}

	function game_continue()
	{
		if ($this->p1->live_ship() == 0 ||  $this->p2->live_ship() == 0)
			return False;
		return True;
	}

}