<?php


//require_once "../models/arena.class.php";
//require_once "../models/spaceships.class.php";

class Arena
{
	public $arena = array();
	public $x = 150;
	public $y = 100;

	function __construct()
	{
		for ($i = 0; $i < $this->y; $i++)
		{
			for ($j = 0; $j < $this->x; $j++)
				$this->arena[$i][$j] = 0;
		}
	}

	public function show_arena()
	{
		?>
		<table style="border-collapse: collapse; background: url('1.jpg'); background-size: cover; color: black">
	<?php
		for ($i = 0; $i < $this->y; $i++)
		{
			echo "<tr>";
			for ($j = 0; $j < $this->x; $j++)
			{
				$color = "";
				$c = "";
				if ($this->arena[$i][$j] > 0)
				{
					$color = $this->color($this->arena[$i][$j] - 1);
					$c = $this->arena[$i][$j];
				}
				echo "<td title='$i-$j' style='font-size: 8px; width: 10px; height: 10px; $color;'> $c </td>";
			}
			echo "</tr>";
		}
		echo "</table>";
	}

	function border($id)
	{
		if ($id < 6)
		{
			return ("border: 1.5px solid 	blue;");
		}
		else
			return ("border: 1.5px solid 	#7FFF00;");

	}

	function color($id)
	{
		$array = array('Khaki', 'DarkCyan', '#50C878', 'pink', 'DarkSalmon', 'IndianRed', 'violet',
			'CornflowerBlue', 'LightCoral', 'OliveDrab', '#ED9121', '#C19A6B');
		$color = $array[$id];
		return ("background-color: $color");
	}

	public function put_ship($ship, $id)
	{
		if ($id == 1)
		{
			for ($i = 0; $i < $this->y; $i++)
			{
				if ($this->arena[$i][0] < 1)
				{
					for ($j = 1; $j < (int)$ship->size / 10; $j++)
					{
						for ($k = 0; $k < (int)$ship->size % 10; $k++)
							$this->arena[$i][$k] = $ship->id;
						$i++;

					}
					return ;
				}
			}
		}
		elseif ($id == 2)
		{
			for ($i = $this->y - 1; $i > 0; $i--)
			{
				if ($this->arena[$i][$this->x-1] < 1)
				{
					$k = 1;
					while ($k < (int)$ship->size / 10)
					{
						$j = $this->x-1;
						$l = 0;
						while ($l < (int)$ship->size % 10)
						{
							$this->arena[$i][$j] = $ship->id;
							$j--;
							$l++;
						}
						$k++;
						$i--;
					}
					return ;
				}
			}
		}
	}

}

//$ship = new Spaceship('Imperial_Frigate_1');
//
//$a = new Arena();
//$a->put_ship($ship, 1);
//$a->put_ship($ship, 1);
//$a->put_ship($ship, 1);
//$a->put_ship($ship, 1);
//$a->show_arena();