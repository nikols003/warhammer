<?php

//	require_once 'db.php';
	require_once "models/player.class.php";
	require_once "models/spaceships.class.php";
	require_once "controllers/arena.class.php";
	require_once "controllers/game.class.php";


	session_start();

	if (isset($_SESSION['game']))
	{
		if (!isset($_POST['submit']))
			$_POST['submit'] = "PP";
		unset($_SESSION['error']);
		if ($_POST['submit'] == 'MOVE') {
			$_POST['submit'] = 'FIRE';
			$_SESSION['game']->move($_POST);
		}
		elseif ($_POST['submit'] == 'FIRE')
		{
			$_POST['submit'] = 'PP';
			$_SESSION['game']->fire($_POST);
		}
		elseif ($_POST['submit'] == 'PP')
		{
			$_POST['submit'] = 'MOVE';
			$_SESSION['game']->pp($_POST);
		}
	}
	else {
		if (!isset($_SESSION['start']))
			header('Location: views/start.php');
		else {
			$ships_1 = array($_SESSION['start']['ship_1'], $_SESSION['start']['ship_2'], $_SESSION['start']['ship_3'],
				$_SESSION['start']['ship_4'], $_SESSION['start']['ship_5'], $_SESSION['start']['ship_6']);
			$ships_2 = array($_SESSION['start']['ship_7'], $_SESSION['start']['ship_8'], $_SESSION['start']['ship_9'],
				$_SESSION['start']['ship_10'], $_SESSION['start']['ship_11'], $_SESSION['start']['ship_12']);

			$p1 = new Player(array('name' => $_SESSION['start']['p1_name'], 'ships' => $ships_1, 'id' => 1));
			$p2 = new Player(array('name' => $_SESSION['start']['p2_name'], 'ships' => $ships_2, 'id' => 2));
			$_POST['submit'] = 'MOVE';
			$game = new Game(array($p1, $p2));
		}
	}
